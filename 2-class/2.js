/*
 * TODO: declare a class Person that will have 2 properties "firstName" and "lastName" and a method
 *  getFullName() that will return a concatenated String of "firstName" and "lastName"
 */



/*
 * TODO#1: map objects in given array to Person class instances and log it to the console.
 * TODO#2: map again received array and utilize "getFullName" method. Log result to the console.
 */
const data = [
  { first: "Nunez", last: "Glover" },
  { first: "Powers", last: "Randolph" },
  { first: "Pauline", last: "Hansen" },
  { first: "Morgan", last: "Sampson" },
  { first: "Wilda", last: "Wilson" },
  { first: "Hughes", last: "Barber" },
];


