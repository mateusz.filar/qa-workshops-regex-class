/*
 * TODO: copy Person class from previous exercise and refactor
 *  "getFullName" method to a getter. Refactor (or write again) the data mapping function.
 */
const data = [
  { first: "Nunez", last: "Glover" },
  { first: "Powers", last: "Randolph" },
  { first: "Pauline", last: "Hansen" },
  { first: "Morgan", last: "Sampson" },
  { first: "Wilda", last: "Wilson" },
  { first: "Hughes", last: "Barber" },
];


