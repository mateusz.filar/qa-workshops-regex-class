/*
 * TODO: write a Person class that will have the following properties:
 *  "name" - name of the person: String
 *  "age" - person age: Number
 *  "_isAdult" - Boolean that will be set automatically (not in constructor)
 *  Requirements:
 *   - "age" should be a setter and on set it will initialize private field "_age" and also "_isAdult"
 *   - if provided age is >= 18 "_isAdult" is set to true, false otherwise
 *   - provide a getter for "age" that will return private "_age" property
 *   - implement a method "drink()" that will console.log based on "_isAdult" property:
 *      true => `🍺`
 *      false => `Not old enough to drink 🤷‍`
 */


