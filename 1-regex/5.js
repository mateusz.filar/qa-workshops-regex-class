const people = [
  { "balance": "1657.22", "name": "Clarke Clarke" },
  { "balance": "3533.08", "name": "Poole Diaz" },
  { "balance": "1051.66", "name": "Kim Rush" },
  { "balance": "2983.76", "name": "Helene Wooten" },
  { "balance": "2515.88", "name": "Blevins Herrera" },
  { "balance": "3230.87", "name": "Keith Chan" },
  { "balance": "1338.72", "name": "Dana Roberson" },
  { "balance": "3441.46", "name": "Fischer Dillard" },
  { "balance": "3495.66", "name": "Guadalupe Moran" }
];

/*
 * TODO: filter people that have more than 2000 balance
 * Required result: 6 people
 */


