const brokenZipNumbers = [
  "87-5622",
  "99A10",
  "59-2943",
  "1-4271",
  "-6-722",
  "27-2614",
  "89173",
  "15-5063",
  "68-6274",
  "743-36",
  "26+789",
  "20-91",
  "41-2108",
  "51-9428",
  "-51787",
  "41-837",
  "71.9671",
  "58-276",
  "32=732",
  "66-687",
];

/*
 * Assumption: Proper ZIP number has a following pattern: XX-XXX
 *   where X in a number from 0 to 9
 *   AND - is OPTIONAL (!!).
 * TODO: With the above assumption filter proper ZIP numbers from "zipNumbers" array
 * Required result: only 4 proper zips [ '89173', '41-837', '58-276', '66-687' ]
 */


