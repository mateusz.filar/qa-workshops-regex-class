const people = [
  { "first": "Bean", "last": "Grant" },
  { "first": "Maryanne", "last": "Knox" },
  { "first": "Corrine", "last": "Baker" },
  { "first": "Maureen", "last": "Cruz" },
  { "first": "Harrison", "last": "Mayer" },
  { "first": "Workman", "last": "Rollins" },
  { "first": "Merritt", "last": "Pacheco" },
  { "first": "Danielle", "last": "Fletcher" },
  { "first": "Maria", "last": "Haley" },
  { "first": "Merle", "last": "Middleton" }
];

/*
 * TODO: refactor filtering code to use RegEx
 * Required result: 5 people
 *
 * TODO#2: do the solution with the usage of only 1 regex
 *  hint: map people to an array of full name strings first
 */

const result = people.filter(person => person.first.startsWith("Ma") || person.first.startsWith("Me"));

console.log(result);
