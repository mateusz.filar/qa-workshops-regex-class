const people = [
  { first: "Bean", last: "Grant" },
  { first: "Maryanne", last: "Knox" },
  { first: "Corrine", last: "Baker" },
  { first: "Maureen", last: "Cruz" },
  { first: "Harrison", last: "Mayer" },
  { first: "Workman", last: "Rollins" },
  { first: "Merritt", last: "Pacheco" },
  { first: "Danielle", last: "Fletcher" },
  { first: "Maria", last: "Haley" },
  { first: "Merle", last: "Middleton" },
];

/*
 * TODO: filter people that first name ends with "e" or "n" and last name ends with "r" or "s"
 * Required result: [ 'Corrine Baker', 'Harrison Mayer', 'Workman Rollins', 'Danielle Fletcher' ]
 *
 * TODO#2: do the solution with the usage of only 1 regex
 *  hint: map people to an array of full name strings first
 */


