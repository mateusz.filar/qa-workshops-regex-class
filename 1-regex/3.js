const text = 'Lorem ipsum dolor sit Amet, Consectetur  lorem adipiscing Elit.';

// HINT: String.match(RegExp) returns an array of matched sub-strings

/*
 * TODO: declare a RegExp and match all capital letters in text variable - should be 4 letters [ 'L', 'A', 'C', 'E' ]
 * Required result: [ 'L', 'A', 'C', 'E' ]
 */



/*
 * TODO: declare a RegExp that will match all words from the text variable - the result should be the 9-elements array
 * of words that don't include spaces, colons and dot characters:
 * Required result: [ 'Lorem', 'ipsum', 'dolor', 'sit', 'Amet',  'Consectetur', 'lorem', 'adipiscing', 'Elit' ]
 */

