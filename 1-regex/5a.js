const people = [
  { "balance": "$1,657.22", "name": "Clarke Clarke" },
  { "balance": "$3,533.08", "name": "Poole Diaz" },
  { "balance": "$1,051.66", "name": "Kim Rush" },
  { "balance": "$2,983.76", "name": "Helene Wooten" },
  { "balance": "$2,515.88", "name": "Blevins Herrera" },
  { "balance": "$3,230.87", "name": "Keith Chan" },
  { "balance": "$1,338.72", "name": "Dana Roberson" },
  { "balance": "$3,441.46", "name": "Fischer Dillard" },
  { "balance": "$32,211.33", "name": "John Doe" },
  { "balance": "$3,495.66", "name": "Guadalupe Moran" }
];

/*
 * TODO: the same task as before - filter people that have more than $2,000 balance -
 *  but the balance has a different format
 * Required result: 7 people
 */


